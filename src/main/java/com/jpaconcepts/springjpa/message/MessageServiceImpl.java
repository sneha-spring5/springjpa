package com.jpaconcepts.springjpa.message;

import com.jpaconcepts.springjpa.model.ProfileMessageResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Service
public class MessageServiceImpl implements MessageService {

    private final MessageRepository messageRepository;

    @Override
    public Message addMessage(Message message) {
        return messageRepository.save(message);
    }

    @Override
    public List<Message> getAllMessage() {
        return (List<Message>) messageRepository.findAll();
    }

    @Override
    public Optional<Message> getMessageById(UUID messageId) {
        return messageRepository.findById(messageId);
    }

    @Override
    public Page<List<ProfileMessageResponse>> getAllMessageAndProfiles() {
        Pageable pageable =
                PageRequest.of(
                        1,
                        1);
        return messageRepository.getAllMessageAndProfiles(pageable);
    }
}
