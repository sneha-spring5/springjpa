package com.jpaconcepts.springjpa.message;


import com.jpaconcepts.springjpa.model.ProfileMessageResponse;
import com.jpaconcepts.springjpa.profile.Profile;
import org.springframework.data.domain.Page;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface MessageService {

   Message addMessage(Message message);

   List<Message> getAllMessage();

   Optional<Message> getMessageById(UUID messageId);

   Page<List<ProfileMessageResponse>> getAllMessageAndProfiles();
}
