package com.jpaconcepts.springjpa.message;

import com.jpaconcepts.springjpa.model.ProfileMessageResponse;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/messages")
public class MessageController {

    private final MessageService messageService;

    @PostMapping
    public Message addMessage(@RequestBody Message message) {
        log.info("Message {} ", message.getEmailId(), " ", message.getUserName(), " ", message.getMessageContent());
        return messageService.addMessage(message);
    }

    @GetMapping
    public List<Message> getAllMessage() {
        return messageService.getAllMessage();
    }

    @GetMapping("/messageId")
    public Message getMessage(@RequestParam(value = "id") UUID messageId) {
        return messageService.getMessageById(messageId).get();
    }

    @GetMapping("/profileandmessages")
    public Page<List<ProfileMessageResponse>> getAllMessageAndProfiles(){
        return messageService.getAllMessageAndProfiles();
    }

}
