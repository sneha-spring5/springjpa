package com.jpaconcepts.springjpa.message;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class Details {

    private Integer expiryDays;
}