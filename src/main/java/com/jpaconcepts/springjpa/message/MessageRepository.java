package com.jpaconcepts.springjpa.message;

import com.jpaconcepts.springjpa.model.ProfileMessageResponse;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface MessageRepository extends CrudRepository<Message, UUID>, PagingAndSortingRepository<Message,UUID> {


    @Query(
            value =
                    "SELECT "
                            + "CAST(m.message_id as varchar) messageId, "
                            + "m.user_name as userName, "
                            + "m.email_id as emailId, "
                            + "m.message_content as messageContent, "
                            + "CAST(p.profile_id as varchar) profileId, "
                            + "p.profile_name as profileName, "
                            + "FROM message m,profile p "
                            + "WHERE m.message_id=p.message_id ",
            countQuery = "select count(m.message_id) from message m,profile p where m.message_id=p.message_id ",
            nativeQuery = true)
    Page<List<ProfileMessageResponse>> getAllMessageAndProfiles(Pageable pageable);
}
