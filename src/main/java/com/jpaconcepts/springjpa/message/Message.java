package com.jpaconcepts.springjpa.message;


import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

@Data
@Entity
public class Message {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "message_id", nullable = false )
    private UUID messageId;

    @Column(name = "user_name", nullable = false ,unique = true)
    private String userName;

    @Column(name = "email_id", nullable = false ,unique = true)
    private String emailId;

    @Column(name = "message_content")
    private String messageContent;

}
