package com.jpaconcepts.springjpa.model;

import java.util.UUID;

public interface ProfileMessageResponse {

    public String getMessageId();
    public String getUserName();
    public String getEmailId();
    public String getMessageContent();
    public String getProfileId();
    public String getProfileName();


}
