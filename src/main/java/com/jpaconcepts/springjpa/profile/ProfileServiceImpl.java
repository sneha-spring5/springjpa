package com.jpaconcepts.springjpa.profile;

import com.jpaconcepts.springjpa.message.Message;
import com.jpaconcepts.springjpa.message.MessageRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;


@RequiredArgsConstructor
@Service
public class ProfileServiceImpl implements ProfileService{

    private final ProfileRepository profileRepository;

    @Override
    public Profile addProfile(Profile profile) {
        return profileRepository.save(profile);
    }

    @Override
    public List<Profile> getAllProfile() {
        return  (List<Profile>) profileRepository.findAll();
    }
}
