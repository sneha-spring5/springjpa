package com.jpaconcepts.springjpa.profile;

import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.util.UUID;

@Data
@Entity
public class Profile {


    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "profile_id", nullable = false)
    private UUID profileId;

    @Column(name = "profile_name", nullable = false, unique = true)
    private String profileName;

    @Column(name = "message_id", nullable = false)
    private UUID messageId;

}
