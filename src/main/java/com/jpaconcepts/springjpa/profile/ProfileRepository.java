package com.jpaconcepts.springjpa.profile;

import com.jpaconcepts.springjpa.message.Message;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;

import java.util.UUID;

public interface ProfileRepository extends CrudRepository<Profile, UUID>, PagingAndSortingRepository<Profile,UUID> {
}
