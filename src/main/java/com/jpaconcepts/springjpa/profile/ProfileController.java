package com.jpaconcepts.springjpa.profile;

import com.jpaconcepts.springjpa.message.Message;
import com.jpaconcepts.springjpa.message.MessageService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.UUID;

@Slf4j
@RequiredArgsConstructor
@RestController
@RequestMapping("/profiles")
public class ProfileController {

    private final ProfileService profileService;

    @PostMapping
    public Profile addProfile(@RequestBody Profile profile) {
        return profileService.addProfile(profile);
    }

    @GetMapping
    public List<Profile> getAllProfile() {
        return profileService.getAllProfile();
    }


}
