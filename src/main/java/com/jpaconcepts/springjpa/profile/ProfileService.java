package com.jpaconcepts.springjpa.profile;


import com.jpaconcepts.springjpa.message.Message;

import java.util.List;

public interface ProfileService {

    Profile addProfile(Profile profile);
    List<Profile> getAllProfile();
}
